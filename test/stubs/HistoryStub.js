import { assert } from 'chai';

import { UNIT_RATE } from '../UserAccountTest';

export class HistoryStub {
    constructor(uncalculatedFees) {
        this.appliedSum = 0;
        this.fees = this.getCalculatedFees();

        uncalculatedFees.forEach((value, key) => this.fees.set(key, value));
    }

    getCalculatedFees() {
        return new Map([
            [new Date(2001, 3, 28), 100],
            [new Date(2001, 4, 18), 150]
        ]);
    }

    getAllFees(tariff, service) {
        return this.fees;
    }

    applyRecalculation(value, unitRate) {
        this.appliedSum = value;
        assert.closeTo(UNIT_RATE, unitRate, HistoryStub.DELTA);
    }

    verifyAppliedSum(expectedSum) {
        assert.closeTo(expectedSum, this.appliedSum, HistoryStub.DELTA);
    }
}

HistoryStub.DELTA = 0.0001;