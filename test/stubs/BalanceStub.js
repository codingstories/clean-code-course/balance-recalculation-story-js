import { assert } from 'chai';

import { HistoryStub } from './HistoryStub';

export class BalanceStub {
    constructor() {
        this.updatedSum = 0;
    }

    updateBalance(sum) {
        this.updatedSum += sum;
    }

    verifyUpdatedSum(expectedSum) {
        assert.closeTo(expectedSum, this.updatedSum, HistoryStub.DELTA);
    }
}